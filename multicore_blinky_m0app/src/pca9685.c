/*
 * pca9685.c
 *
 *  Created on: 31 août 2014
 *      Author: ktown
 */

#include <math.h>
#include "pca9685.h"
#include "board.h"

#define I2C_CLK_DIVIDER        (40)
#define I2C_BITRATE            (100000)

static uint8_t       m_pca9685_i2caddr;                   /* I2C device address placeholder */
//static I2C_HANDLE_T *m_pca9685_i2cHandleMaster;           /* I2C master handle and memory for ROM API */
static uint32_t      m_pca9685_i2cMasterHandleMEM[0x20];  /* I2C buffer */
static I2CM_XFER_T i2cmXferRec;
/**************************************************************************/
/*!
    @brief  Reads the value of the specified 8-bit register

    @param  addr
            The address of the register that should be read

    @return The contents of the register
*/
/**************************************************************************/
static void SetupXferRecAndExecute(uint8_t devAddr,
								   uint8_t *txBuffPtr,
								   uint16_t txSize,
								   uint8_t *rxBuffPtr,
								   uint16_t rxSize)
{
	/* Setup I2C transfer record */
	i2cmXferRec.slaveAddr = devAddr;
	i2cmXferRec.options = 0;
	i2cmXferRec.status = 0;
	i2cmXferRec.txSz = txSize;
	i2cmXferRec.rxSz = rxSize;
	i2cmXferRec.txBuff = txBuffPtr;
	i2cmXferRec.rxBuff = rxBuffPtr;
	Chip_I2CM_Xfer(LPC_I2C0, &i2cmXferRec);

	/* Wait for transfer completion */
	WaitForI2cXferComplete(&i2cmXferRec);
}

uint8_t pca9685Read8(uint8_t addr)
{
  uint8_t sendData[10] = { 0 };
  uint8_t recvData[10] = { 0 };


  //sendData[0] = m_pca9685_i2caddr;
  sendData[0] = addr;
  recvData[0] = m_pca9685_i2caddr | 0x01;

  /* Setup I2C parameters */
  SetupXferRecAndExecute(m_pca9685_i2caddr, sendData, 2, recvData, 2);

  return recvData[1];
}

/**************************************************************************/
/*!
    @brief  Updates the value of the specified 8-bit register

    @param  addr
            The address of the register that should be updated
    @param  value
            The value to write to the register at 'addr'
*/
/**************************************************************************/
void pca9685Write8(uint8_t addr, uint8_t value)
{
  uint8_t      sendData[3] = { 0, 0, 0 };
  sendData[0] = addr;
  sendData[1] = value;
  SetupXferRecAndExecute(m_pca9685_i2caddr, sendData, 2, NULL, 0);
}

/**************************************************************************/
/*!
    @brief  Sets the PWM duty cycle for a single channel.

    @param  channel
            The PWM channel to update (0..15)
    @param  on
            The 'high' time (0..4096)
    @param  off
            The 'low' time (0..4096)
*/
/**************************************************************************/
void pca9685SetPWM(uint8_t channel, uint16_t on, uint16_t off)
{
  uint8_t      tx_buffer[6];
  ErrorCode_t  error_code;

  if (on > 4095)
  {
    on = 4096;
  }
  if (off > 4095)
  {
    off = 4096;
  }

//  tx_buffer[0] = m_pca9685_i2caddr;
  tx_buffer[0] = PCA9685_REG_LED0_ON_L + 4 * channel;
  tx_buffer[1] = (uint8_t)on;
  tx_buffer[2] = (uint8_t)(on >> 8);
  tx_buffer[3] = (uint8_t)off;
  tx_buffer[4] = (uint8_t)(off >> 8);

  SetupXferRecAndExecute(m_pca9685_i2caddr, tx_buffer, 5, NULL, 0);
}

/**************************************************************************/
/*!
    @brief  Sets the base frequency (in Hz) for the PWM outputs

    @param  freq
            Base frequency (in Hz) for the PWM outputs
*/
/**************************************************************************/
void pca9685SetPWMFreq(float freq)
{
  float prescaleval = 25000000;
  prescaleval /= 4096;
  prescaleval /= freq;
  prescaleval -= 1;

  uint8_t prescale = floor(prescaleval + 0.5);
  uint8_t oldmode = pca9685Read8(PCA9685_REG_MODE1);
  uint8_t newmode = (oldmode & 0x7F) | 0x10;            // sleep
  pca9685Write8(PCA9685_REG_MODE1, newmode);         // go to sleep
  pca9685Write8(PCA9685_REG_PRESCALE, prescale);     // set the prescaler
  pca9685Write8(PCA9685_REG_MODE1, oldmode);         // wake up again
  /* ToDo: Add 5ms HW delay */
  uint16_t x = 0xFFFF;
  while(x--)
  {
    __asm volatile("nop");
  }
  pca9685Write8(PCA9685_REG_MODE1, oldmode | 0xa1);  // turn on auto increment
}


/**************************************************************************/
/*!
    @brief  Initialises the I2C bus (I2C0) in Master mode, and resets the
            outputs on the PCA9685 PWM driver

    @param  addr
            The 7-bit I2C address to use when addressing the PCA9685
*/
/**************************************************************************/
bool pca9685Init(uint8_t addr)
{
  m_pca9685_i2caddr = addr;

  /* Setup the pinmux for I2C pins */
#if defined(BOARD_NXP_LPCXPRESSO_824)
  Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_SWM);
  Chip_SWM_EnableFixedPin(SWM_FIXED_I2C0_SDA);
  Chip_SWM_EnableFixedPin(SWM_FIXED_I2C0_SCL);
#if (I2C_BITRATE > 400000)
  /* Enable Fast Mode Plus for I2C pins */
  Chip_IOCON_PinSetI2CMode(LPC_IOCON, IOCON_PIO10, PIN_I2CMODE_FASTPLUS);
  Chip_IOCON_PinSetI2CMode(LPC_IOCON, IOCON_PIO11, PIN_I2CMODE_FASTPLUS);
#endif
  Chip_Clock_DisablePeriphClock(SYSCTL_CLOCK_SWM);
#else
  /* Configure your own I2C pin muxing here if needed */
  Board_I2C_Init(I2C1);
  Chip_I2C_Init(I2C1);
  Chip_I2C_SetClockRate(I2C1, 100000);
  i2c_set_mode(I2C1, 0);
#endif


  /* ToDo: Ping the PCA9685 to make sure it exists */

  /* Reset the PCA9685 and disable all PWM outputs */
  pca9685Write8(PCA9685_REG_MODE1, 0x0);
  pca9685SetPWMFreq(1600);
  uint8_t i;
  for (i=0; i<16; i++)
  {
    pca9685SetPWM(i, 0, 0);
  }

  return true;
}

void I2C0_IRQHandler(void)
{
	/* Call I2CM ISR function with the I2C device and transfer rec */
	Chip_I2CM_XferHandler(LPC_I2C0, &i2cmXferRec);
}
