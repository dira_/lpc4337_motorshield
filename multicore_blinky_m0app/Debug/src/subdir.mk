################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/cr_startup_lpc43xx-m0app.c \
../src/motorshield.c \
../src/multicore_blinky.c \
../src/pca9685.c \
../src/sysinit.c 

OBJS += \
./src/cr_startup_lpc43xx-m0app.o \
./src/motorshield.o \
./src/multicore_blinky.o \
./src/pca9685.o \
./src/sysinit.o 

C_DEPS += \
./src/cr_startup_lpc43xx-m0app.d \
./src/motorshield.d \
./src/multicore_blinky.d \
./src/pca9685.d \
./src/sysinit.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -DDEBUG -D__CODE_RED -DCORE_M0 -D__USE_LPCOPEN -D__LPC43XX__ -D__REDLIB__ -D__MULTICORE_M0APP -DCORE_M0APP -I"C:\Users\JGBDesktop\Google Drive\DIscere\Code\lpc4337_motorshield\lpc4337_motorshield\lpc_board_nxp_lpcxpresso_4337_m0\inc" -I"C:\Users\JGBDesktop\Google Drive\DIscere\Code\lpc4337_motorshield\lpc4337_motorshield\lpc_chip_43xx_m0\inc" -I"C:\Users\JGBDesktop\Google Drive\DIscere\Code\lpc4337_motorshield\lpc4337_motorshield\lpc_chip_43xx_m0\inc\config_43xx_m0app" -I"C:\Users\JGBDesktop\Google Drive\DIscere\Code\lpc4337_motorshield\lpc4337_motorshield\lpc_chip_43xx_m0\inc\usbd_rom" -I"C:\Users\JGBDesktop\Google Drive\lpc4337_motorshield\lpc4337_motorshield\multicore_blinky_m0app\inc" -I"C:\Users\JGBDesktop\Google Drive\lpc4337_motorshield\lpc4337_motorshield\multicore_blinky_m0app\multicore_common" -O0 -fno-common -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m0 -mthumb -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


